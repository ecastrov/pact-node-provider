const { Verifier } = require('@pact-foundation/pact');
const packageJson = require('../package.json');
const stateHandlers = require('./stateHandlers');

let opts = {
  providerBaseUrl: 'http://localhost:3000',
  provider: 'hero-provider',
  publishVerificationResult: true,
  providerVersion: packageJson.version,
  stateHandlers
};

//locally
opts.pactUrls = [
  '/home/ecastro/Proyectos/pact-node-provider/pact/hero-consumer-hero-provider.json'
];

//Broker
//opts.pactBrokerUrl = 'https://adesso.pact.dius.com.au';
//opts.pactBrokerUsername = process.env.PACT_USERNAME;
//opts.pactBrokerPassword = process.env.PACT_PASSWORD;

new Verifier().verifyProvider(opts).then(() => {
  console.log('Pacts successfully verified!');
});
