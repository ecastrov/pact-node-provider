const express = require('express');
const app = express();

const {
  ENVIRONMENT = 'development',
  SERVER_NAME = 'node-provider'
} = process.env;

app.get('/', (req, res, next) => {
  res.send({
    serverName: SERVER_NAME,
    env: ENVIRONMENT
  });
});

module.exports = app;
